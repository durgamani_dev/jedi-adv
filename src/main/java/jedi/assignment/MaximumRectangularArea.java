package jedi.assignment;

import java.util.Stack;

public class MaximumRectangularArea {

    public static int maxRectangularArea(int[] arr){
        int maxArea = 0;
        Stack<Integer> stack = new Stack<>();
        for(int i =0;i<arr.length;i++) {
            int start = i;
            while (!stack.isEmpty() && arr[stack.peek()] > arr[i]) {
                int height = arr[stack.peek()];
                int width = i - stack.pop();
                maxArea = Math.max(maxArea, height * width);

                start = i;
            }
            stack.add(start);
        }
        while(!stack.isEmpty()){
            int height = arr[stack.peek()];
            int start = stack.pop();
            int area =height * (arr.length - start);
            maxArea  = Math.max(area,maxArea);
        }
        return maxArea;
    }

    public static void main(String args[]){
        int[] arr = {2,1,4,7,5,3};
        System.out.println("max area : "+maxRectangularArea(arr));
    }
}
