package jedi.assignment;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UnDirectedCyclicGraph {
    int V;
    List<List<Integer>>  edges = new ArrayList<>(V);
    public UnDirectedCyclicGraph(int v){
        this.V = v;
        for(int i =0;i<V;i++)
            edges.add(new LinkedList<>());
    }

    public boolean isCyclicUtil(int s,int parent,boolean[] visited){
        visited[s] = true;
        List<Integer> children = edges.get(s);
        for(int c : children){
            if(!visited[c]){
                if(isCyclicUtil(c,s,visited))return true;
            }else if(c != parent) return true;
        }
        return false;
    }

    public boolean isCyclic()
    {
        boolean[] visited = new boolean[V];
        for(int i =0;i<V;i++)
            visited[i] = false;
        for(int i=0;i<V;i++) {
            if (!visited[i] && isCyclicUtil(i, -1, visited)) return true;
        }
            return false;
    }

    public void addEdge(int s,int d){
        edges.get(s).add(d);
        edges.get(d).add(s);
    }

    public static void main(String args[]){
        UnDirectedCyclicGraph graph = new UnDirectedCyclicGraph(4);
        graph.addEdge(0,1);
        graph.addEdge(0,2);
        graph.addEdge(1,3);
        graph.printGraph(graph);
        graph.addEdge(3,0);
        graph.printGraph(graph);
    }

    private  void printGraph(UnDirectedCyclicGraph graph) {
        if (graph.isCyclic())
            System.out.println("cyclic graph");
        else
            System.out.println("not cyclic graph");
    }
}
