package jedi.assignment;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SimpleGraph {

    private Map<String, List<String>> map = new HashMap<>();

    public void addVertex(String s){
        map.put(s, new LinkedList<String>());
    }
    public void addEdge(String s,String d,boolean bidirection){
        if(!map.containsKey(s)) addVertex(s);
        if(!map.containsKey(d))addVertex(d);
        map.get(s).add(d);
        if(bidirection == true){
            map.get(d).add(s);
        }
    }

    public void getVertexCount(){
        System.out.println("the graph has" + map.keySet().size() + "vertices");
    }
    public void getEdgesCount(boolean bidir){
        int count = 0;
        for(String s : map.keySet()){
            count += map.get(s).size();
        }
        if(bidir == true){
            count = count/2;
        }
        System.out.println("The graph has" + count + "edges ");
    }
    public  void hasVertex(String s){
        if(map.containsKey(s)){
            System.out.println("the graph has "+ s + "vertex");
        }else{
            System.out.println("the graph does not contain" + s + "vertex");
        }
    }

    public void hasEdge(String s, String d){
        if(map.get(s).contains(d)){
            System.out.println("graph contains the edge form " + s +"to" + d);
        }else{
            System.out.println("graph doesn't have the edge form " + s +"to" + d);
        }
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        map.forEach((key,value) -> {
            builder.append( "\n" +key + ":");
            value.forEach(builder::append);
        });
        return builder.toString();
    }

    public static void main(String args[]){
        SimpleGraph g = new SimpleGraph();
        g.addEdge("0","1",true);
        g.addEdge("0","4",true);
        g.addEdge("1","2",true);
        g.addEdge("1","3",true);
        g.addEdge("1","4",true);
        g.addEdge("2","3",true);
        g.addEdge("3","4",true);

        System.out.println("Graph:\n"
                + g.toString());

        // gives the no of vertices in the graph.
        g.getVertexCount();

        // gives the no of edges in the graph.
        g.getEdgesCount(true);

        // tells whether the edge is present or not.
        g.hasEdge("3", "4");

        // tells whether vertex is present or not
        g.hasVertex("5");
    }
}

