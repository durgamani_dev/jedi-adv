package jedi.assignment;

import java.util.Arrays;

public class LongestSubSeqSol2 {
    public static int longSubSeq(int a[]){
        int[] lis = new int[a.length];
        int max =0;
        Arrays.fill(lis,1);
        for(int i=1;i<a.length;i++){
            for(int j = 0;j<i;j++){
                if(a[i]>a[j] && lis[i]<lis[j]+1)lis[i] = lis[j] +1;
            }
        }
        for(int i=0;i<a.length;i++){
            if(max<lis[i]) max = lis[i];
        }
        return max;
    }

    public static void main(String[] args) {
        int[] a = {3,4,-1,5,8,2,3,12,7,9,10};
        System.out.println("LIS is : "+ longSubSeq(a));
    }
}
