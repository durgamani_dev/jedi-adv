package jedi.assignment;

public class KadanesAlgorithm {

    public static  int maxSubArraySum(int[] a){
        int start =0;
        int end = 0;
        int s =0;
        int sum =0;
        int max_sum =0;
        for(int i=0;i<a.length;i++){
            sum += a[i];
            if(max_sum<sum){
                max_sum = sum;
                start = s; end = i;
            }
            if(sum<0){
                sum = 0;
                s = i + 1;
            }
        }
        return max_sum;
    }

    public static  void main(String args[]){
        int[] a = {4,-3,-2,2,3,1,-2,-3,4,2,-6,-3,-1,3,1,2};
        System.out.println(maxSubArraySum(a));
    }
}
