package jedi.assignment;

import java.util.HashMap;
import java.util.Map;

public class Trie {
    private TreeNode root;

    class TreeNode {
        private Map<Character, TreeNode> children = new HashMap<>();
        boolean endOfWord;

        public Map<Character, TreeNode> getChildren() {
            return children;
        }

        public void setChildren(Map<Character, TreeNode> children) {
            this.children = children;
        }

        public boolean isEndOfWord() {
            return endOfWord;
        }

        public void setEndOfWord(boolean endOfString) {
            this.endOfWord = endOfString;
        }


    }


    public void insert(String word) {
        if (root == null) {
            root = new TreeNode();
        }
        TreeNode current = root;
        for (char c : word.toCharArray()) {
            current = current.getChildren().computeIfAbsent(c, l -> new TreeNode());
        }
        current.setEndOfWord(true);
    }

    public boolean find(String word) {
        TreeNode current = root;
        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            TreeNode node = current.getChildren().get(ch);
            if (node == null) {
                return false;
            }
            current = node;
        }
        return current.isEndOfWord();
    }

    public void delete(String word) {
        delete(root, word, 0);
    }

    public boolean delete(TreeNode current, String word, int index) {
        if (index == word.length()) {
            if (!current.isEndOfWord()) {
                return false;
            }
            current.setEndOfWord(false);
            return current.getChildren().isEmpty();
        }
        char ch = word.charAt(index);
        TreeNode node = current.getChildren().get(ch);
        if (node == null) {
            return false;
        }
        boolean shouldDeleteCurrentNode = delete(node, word, index + 1) && !node.isEndOfWord();
        if (shouldDeleteCurrentNode) {
            current.getChildren().remove(ch);
            return current.getChildren().isEmpty();
        }
        return false;
    }

    public static void main(String args[]) {
        Trie trie = new Trie();
        trie.insert("the");
        trie.insert("theme");
        if (trie.find("theme")) {
            System.out.println("theme is found");
        }
        if (!trie.find("th")) {
            System.out.println("th word is not there");
        }
        trie.delete("theme");
        if (!trie.find("theme")) {
            System.out.println("theme word is not there");
        }
        if (trie.find("the")) {
            System.out.println("the word is  there");
        }
    }
}
