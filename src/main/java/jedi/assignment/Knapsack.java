package jedi.assignment;

import java.util.Arrays;

public class Knapsack {
    int max(int a,int b){
        return a>b?a:b;
    }

    public int knapsac(int[] wt,int W,int val[],int i,int[][] dp){
        if(i <0) return 0;
        if(dp[i][W] != -1) return dp[i][W];
        if(wt[i] > W){
            dp[i][W] = knapsac(wt,W,val,i-1,dp);
        }else{
            dp[i][W] = max(val[i] + knapsac(wt,W-wt[i],val,i-1,dp),knapsac(wt,W,val,i-1,dp));
        }
        return dp[i][W];
    }

    public static void main(String[] args) {
        Knapsack ns = new Knapsack();
        int val[] = { 10, 20, 30 };
        int wt[] = { 1, 1, 1 };
        int W = 2;
        int n = val.length;
        int dp[][] = new int[n][W+1];
        for(int i=0;i<n;i++)
        Arrays.fill(dp[i],-1);
        System.out.println(ns.knapsac(wt,W,val,n-1,dp));
    }
}
