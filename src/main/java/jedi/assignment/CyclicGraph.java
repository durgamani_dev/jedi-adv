package jedi.assignment;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CyclicGraph {
    int V;
    List<List<Integer>> edges;
    public CyclicGraph(int v){
        this.V = v;
        edges = new ArrayList<>(V);
        for(int i=0;i<V;i++)
            edges.add(new LinkedList<>());
    }

    public boolean isCyclicUtils(int s,boolean[] visited,boolean[] rStack){
        if(rStack[s]) return true;
        if(visited[s]) return false;
        visited[s] = true;
        rStack[s] = true;
        List<Integer> children = edges.get(s);
        for(Integer c: children){
            if(isCyclicUtils(c,visited,rStack))return true;
        }
        rStack[s] = false;
        return false;
    }

    public void addEdge(int s,int d){
        edges.get(s).add(d);
    }

    public boolean isCyclic(){
        boolean[] visited = new boolean[V];
        boolean[] rStack = new boolean[V];
        for(int i =0;i<V;i++)
        if(isCyclicUtils(i,visited,rStack)) return true;
        return false;
    }

    public static void main(String args[]){
        CyclicGraph graph = new CyclicGraph(4);
        graph.addEdge(0,1);
        graph.addEdge(0,2);
        graph.addEdge(1,2);
        graph.addEdge(2,3);
        graph.printGraph(graph);
        graph.addEdge(3,0);
        graph.printGraph(graph);

    }

    private  void printGraph(CyclicGraph graph) {
        if (graph.isCyclic())
            System.out.println("cyclic graph");
        else
            System.out.println("not cyclic graph");
    }
}
