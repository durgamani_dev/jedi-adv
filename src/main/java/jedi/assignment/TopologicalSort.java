package jedi.assignment;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

public class TopologicalSort {
    int v;
    LinkedList<Integer>[] adj;
    public TopologicalSort(int v){
        this.v = v;
        adj = new LinkedList[v];
        for(int i=0;i<v;i++)
            adj[i] = new LinkedList<>();

    }

    public void addEdge(int s, int v){
        adj[s].add(v);
    }

    public void topologicalSortUtil(int s, boolean[] visited, Stack stack){
        visited[s] = true;
        Iterator<Integer> i = adj[s].listIterator();
        while(i.hasNext()){
            int n = i.next();
            if(!visited[n])
                topologicalSortUtil(n,visited,stack);
        }
        stack.push(s);
    }

    public void topologicalSort(){
        boolean[] visited = new boolean[v];
        Stack<Integer> stack = new Stack<>();
        for(int i =0;i< v;i++){
            visited[i] = false;
        }
        for(int i =0;i<v;i++){
            if(!visited[i])
                topologicalSortUtil(i,visited,stack);
        }

        while(!stack.empty())
            System.out.println(stack.pop());
    }

    public static void main(String args[]){
        TopologicalSort t = new TopologicalSort(6);
        t.addEdge(5, 2);
        t.addEdge(5, 0);
        t.addEdge(4, 0);
        t.addEdge(4, 1);
        t.addEdge(2, 3);
        t.addEdge(3, 1);

        System.out.println("Following is a Topological " +
                "sort of the given graph");
        t.topologicalSort();
    }
}
