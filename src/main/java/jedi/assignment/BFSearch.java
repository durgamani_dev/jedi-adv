package jedi.assignment;

import java.util.Iterator;
import java.util.LinkedList;

public class BFSearch {

    int v;
    LinkedList<Integer>[] adj;
    BFSearch(int v){
        this.v = v;
        adj = new LinkedList[v];
        for(int i =0;i<v;i++)
            adj[i] = new LinkedList<>();
    }
    void addEdge(int s, int d){
        adj[s].add(d);
    }


    void bfs(int s){
        boolean[] visited = new boolean[v];
        LinkedList<Integer> queue = new LinkedList<>();
        queue.add(s);
        visited[s] = true;

        while (!queue.isEmpty()){
            s = queue.poll();
            System.out.println(s);
            Iterator<Integer> iterator = adj[s].listIterator();
            while (iterator.hasNext()){
                int n = iterator.next();
                if(!visited[n]){
                    queue.add(n);
                    visited[n] = true;
                }
            }
        }
    }

    public static void  main(String args[]){
        BFSearch bfSearch = new BFSearch(4);
        bfSearch.addEdge(0,1);
        bfSearch.addEdge(0,2);
        bfSearch.addEdge(1,2);
        bfSearch.addEdge(2,0);
        bfSearch.addEdge(2,3);
        bfSearch.addEdge(3,3);

        System.out.println("following is the breadth first traversal starting form vertex 2");
        bfSearch.bfs(2);
    }


}
