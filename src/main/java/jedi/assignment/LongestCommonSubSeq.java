package jedi.assignment;

import java.util.Arrays;

public class LongestCommonSubSeq {
    int max(int a,int b){
        return a>b?a :b;
    }
    public int lcs(char[] x,char[] y,int m,int n,int[][] dp){
        if(m ==0 || n ==0)return 0;
        if(dp[m][n]>0) return dp[m][n];
        if(x[m-1] == y[n-1]) {
            dp[m][n] = 1 + lcs(x, y, m - 1, n - 1,dp);
            return dp[m][n];
        }
        else{
            dp[m][n] =  max(lcs(x,y,m-1,n,dp),lcs(x,y,m,n-1,dp));
            return  dp[m][n];
        }
    }

    public static void main(String[] args) {

    LongestCommonSubSeq l = new LongestCommonSubSeq();
    String s1 = "AGGTAB";
    String s2 = "GXTXAYB";

    char[] X=s1.toCharArray();
    char[] Y=s2.toCharArray();
    int m = X.length;
    int n = Y.length;
    int[][] dp = new int[m+1][n+1];
    for(int i=0;i<m;i++)
        Arrays.fill(dp[i],0);
    System.out.println("Length of LCS is" + " " +l.lcs( X, Y, m, n,dp ) );
    }
}
