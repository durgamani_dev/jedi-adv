package jedi.assignment;

import java.util.Arrays;

import com.sun.tools.internal.xjc.reader.xmlschema.BindPurple;

public class LongestSubSequence {

    private int calculateIndex(int a[],int temp[],int end,int s){
        int start = 0;
        int middle;
        int len = end;
        while(start<=end){
            middle = (start + end) /2;
            if(middle < len && a[temp[middle]] < s && s <= a[temp[middle + 1]]){
                return middle + 1;
            }else if(a[temp[middle]] < s){
                start = middle + 1;
            }else{
                end = middle - 1;
            }
        }
        return -1;
    }

    public int longestIncSubSeq(int[] a){
        int[] temp = new int[a.length];
        int[] result = new int[a.length];
        Arrays.fill(result,-1);
        temp[0] = 0;
        int len = 0;
        for(int i=1;i<a.length;i++){
            if(a[temp[0]]>a[i]){
                temp[0] = i;
            }else if(a[temp[len]] <a[i]){
                len++;
                temp[len] = i;
                result[i] = temp[len - 1];

            }else{
                int index = calculateIndex(a,temp,len,a[i]);
                temp[index] = i;
                result[i] = temp[index-1];
            }
        }
        System.out.println("Longest sub sequence is :");
        int index = temp[len];
        while(index != -1){
            System.out.print(" " + a[index]);
            index = result[index];
        }
        System.out.println();
        return len+1;
    }

    public static void main(String[] args) {
        LongestSubSequence l = new LongestSubSequence();
        int[] a = {3,4,-1,5,8,2,3,12,7,9,10};
        System.out.println("LIS is : "+ l.longestIncSubSeq(a));
    }
}
