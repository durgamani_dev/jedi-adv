package jedi.assignment;

public class RabinKarpSearch {

    private int prime = 3;

    public int patternSearch(char[] text, char[] pattern){
        int m = pattern.length;
        int n = text.length;
        long patternHash = createHash(pattern,m-1);
        long textHash = createHash(text,m-1);
        for(int i=1;i<=n-m+1;i++){
            if(patternHash == textHash && isEqual(text,i-1,i+m-2,pattern,0,m-1)){
                return i-1;
            }
            if(i<n-m+1){
                textHash = reCalculateHash(text,i-1,i+m-1,textHash,m);
            }
        }
        return -1;
    }
    private long reCalculateHash(char[] str,int oldIndex,int newIndex,long oldHash,int patternLen){
        long newHash = oldHash - str[oldIndex];
        newHash = newHash/prime;
        newHash += (long) (str[newIndex]*Math.pow(prime,patternLen-1));
        return newHash;
    }
   private  long createHash(char[] str,int len){
        long hash=0;
        for(int i=0;i<=len;i++){
            hash = hash + (long) (str[i] * Math.pow(prime,i));
        }

        return hash;
    }

    private boolean isEqual(char[] str1,int start1,int end1,char[] str2,int start2,int end2 ){
        if(end1 - start1 != end2 - start2) return false;
        while(start1<=end1 && start2<=end2){
            if(str1[start1] != str2[start2])return false;
            start1++;
            start2++;
        }
        return true;
    }

    public static void main(String args[]){
        RabinKarpSearch rks = new RabinKarpSearch();
        System.out.println(rks.patternSearch("NagaDurgaMani".toCharArray(), "ga".toCharArray()));
        System.out.println(rks.patternSearch("NagaDurgaMani".toCharArray(), "Durga".toCharArray()));
        System.out.println(rks.patternSearch("NagaDurgaMani".toCharArray(), "Mani".toCharArray()));
        System.out.println(rks.patternSearch("NagaDurgaMani".toCharArray(), "gaDurga".toCharArray()));
        System.out.println(rks.patternSearch("NagaDurgaMani".toCharArray(), "gaMa".toCharArray()));
    }

}
