package jedi.assignment;

public class AVLTreeMinNodes {
        static int avlNodes(int height){
            if(height == 0) return 1;
            else if(height == 1)return 2;
            return 1+ avlNodes(height-1) +avlNodes(height-2);
        }
        public static void main(String args[]){
            int h = 3;
            System.out.println("minimum no of nodes:"+avlNodes(h));
        }
}
