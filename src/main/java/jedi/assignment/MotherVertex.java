package jedi.assignment;

import java.util.Iterator;
import java.util.LinkedList;

public class MotherVertex {
    int v;
    LinkedList<Integer>[] adj;
    int visitedNodes =0;
    public MotherVertex(int v){
        this.v = v;
        adj = new LinkedList[v];
        for(int i =0;i<v;i++)
            adj[i] = new LinkedList<>();
    }

    public void addEdge(int s, int d){
        adj[s].add(d);
    }

    public void dfsUtil(int s,boolean[] visited){
        visited[s] = true;
        System.out.println(s);
        visitedNodes++;
        Iterator<Integer> i = adj[s].listIterator();
        while(i.hasNext()){
            int n = i.next();
            if(!visited[n]){
                dfsUtil(n,visited);
            }
        }
    }

    public void dfs(int s){
        visitedNodes = 0;
        boolean[] visited = new boolean[v];
        for(int i=0;i<v;i++)
            visited[i] = false;
        dfsUtil(s,visited);
    }

    public static void main(String args[]){
        int n = 4;
        MotherVertex mv = new MotherVertex(n);
        mv.addEdge(0,1);
        mv.addEdge(0,2);
        mv.addEdge(1,2);
        mv.addEdge(2,0);
        mv.addEdge(2,3);
        mv.addEdge(3,3);
        System.out.println("Following is depth first traversal starting from vertex ");
        int i;
        for(i=0;i<mv.v;i++){
            mv.dfs(i);
            if(mv.visitedNodes == mv.v)
                break;
        }
        if(i != mv.v){
            System.out.println("Mother Vertex is "+ i);
        }else{
            System.out.println("no mother vertex");
        }


    }
}
