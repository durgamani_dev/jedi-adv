package jedi.assignment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NonAdjacentFlowerPlanting {
    Map<Integer, List<Integer>> graph= new HashMap<>();
    public  int[] gardenNoAdj(int N, int[][] paths) {

        int[] flowers = new int[N];
        Arrays.fill(flowers,-1);
        for(int[] path:paths){
            addAdj(path[0],path[1]);

        }

        for(int garden =1;garden<=N;garden++){
            if(!graph.containsKey(garden)){
                flowers[garden-1] = 1;
            }else{
                boolean[] flowersPlaced = new boolean[4];
                getAdjFlowers(flowers, garden, flowersPlaced);
                placeFlowers(flowers, garden, flowersPlaced);
            }

        }
        return flowers;
    }

    private void placeFlowers(int[] flowers, int garden, boolean[] flowersPlaced) {
        for(int flower = 0;flower <4;flower++){
            if(!flowersPlaced[flower]) {
                flowers[garden - 1] = flower+1;
                break;
            }

        }
    }

    private void getAdjFlowers(int[] flowers, int garden, boolean[] flowersPlaced) {
        for(int nextGarden:graph.get(garden)){
            if(flowers[nextGarden-1]>-1){
                flowersPlaced[flowers[nextGarden-1]-1] = true;
            }
        }
    }

    private  void addAdj(int s,int d){
        List<Integer> outNodes = graph.getOrDefault(s,new ArrayList<>());
        outNodes.add(d);
        graph.put(s,outNodes);
        outNodes = graph.getOrDefault(d,new ArrayList<>());
        outNodes.add(s);
        graph.put(d,outNodes);
    }

        public static void main(String args[]){
            NonAdjacentFlowerPlanting f = new NonAdjacentFlowerPlanting();
            int N = 3;
            int[][] paths = { {1,2}, {2,3},{3,1}};
            int[] flowers = f.gardenNoAdj(N,paths);
            for(int i=0;i<N;i++)
            System.out.println(flowers[i]);

        }
}
