package jedi.assignment;

public class AVLTreeGreaterNodes {

    class Node{
        int key;
        Node left,right;
        int height;
        int desc;

        Node(int key){
            this.key = key;
            this.left = null;
            this.right = null;
            this.height = 1;
            this.desc = 0;
        }
    }

    int height(Node node){
        return node == null ? 0 :node.height;
    }

    int max(int a, int b){
        return a>b ? a:b;
    }

    public Node rightRotate(Node y){
        Node x = y.left;
        Node z = x.right;

        x.right = y;
        y.left = z;
        y.height = max(height(y.left),height(y.right)) + 1;
        x.height = max(height(x.left),height(x.right)) + 1;

        int val = (z != null) ? z.desc : -1;
        y.desc = y.desc - (x.desc + 1) + (val+1);
        x.desc = x.desc - (val +1 ) + (y.desc + 1);

        return x;
    }

    public Node leftRotate(Node x){
        Node y = x.right;
        Node z = y.left;

        y.left = x;
        x.right = z;

        x.height = max(height(x.left),height(x.right)) + 1;
        y.height = max(height(y.left),height(y.right)) + 1;

        int val = (z != null) ? z.desc : -1;
        x.desc = x.desc - (y.desc + 1) + (val + 1);
        y.desc = y.desc - (val + 1) + (x.desc + 1);
        return y;
    }

    int getBalance(Node node){
        return node == null ? 0 :height(node.left) - height(node.right);
    }

    Node insert(Node node,int key){
        if(node == null) return new Node(key);

        if(key <node.key) {
            node.left = insert(node.left,key);
            node.desc++;
        }else if(key > node.key) {
            node.right = insert(node.right, key);
            node.desc++;
        }

        node.height = 1 + max(height(node.left), height(node.right));
        int balance = getBalance(node);
        if(balance > 1 && key < node.left.key) return rightRotate(node);
        if(balance < -1 && key > node.right.key) return leftRotate(node);
        if(balance > 1 && key > node.left.key) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }
        if(balance < -1 && key < node.right.key){
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }
        return node;
    }

    void preOrder(Node root){
        if(root != null){
            System.out.println(root.key);
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    int countGreater(Node root,int x){
        int res = 0;
        while(root != null) {
            int desc = (root.right != null)? root.right.desc : -1 ;
            if(root.key > x) {
                res = res + desc + 1 +1 ;
                root = root.left;
            }else if(root.key<x)root = root.right;
            else{
                res = res + desc + 1;
                break;
            }
        }

        return res;
    }
    public static void main(String args[]){
        AVLTreeGreaterNodes tree = new AVLTreeGreaterNodes();
        Node root = null;
        root = tree.insert(root,9);
        root = tree.insert(root, 5);
        root = tree.insert(root, 10);
        root = tree.insert(root, 0);
        root = tree.insert(root, 6);
        root = tree.insert(root, 11);
        root = tree.insert(root, -1);
        root = tree.insert(root, 1);
        root = tree.insert(root, 2);

        System.out.println("preorder traversal of constructing avl tree is");
        tree.preOrder(root);
        System.out.println("Nodes greater than  5 are:"+ tree.countGreater(root,5));
    }

}
