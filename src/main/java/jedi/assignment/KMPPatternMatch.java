package jedi.assignment;

public class KMPPatternMatch {

    public boolean search(char[] text, char[] pattern){
        int[] ptemp = patternTempArray(pattern);
        int i =0;
        int j =0;
        while(i< text.length && j< pattern.length){
            if(text[i] == pattern[j]){
                i++;
                j++;
            }else {
                if(j!=0){
                    j = ptemp[j-1];
                }else{
                    i++;
                }
            }
        }

        if(j == pattern.length) return true;
        return false;
    }

    public int[] patternTempArray(char[] pattern){
        int[] ptemp = new int[pattern.length];
        int j=0;
        for(int i=1;i<pattern.length;){
            if(pattern[i] == pattern[j]){
                ptemp[i] = j +1;
                j++;i++;
            }else{
                if(j !=0){
                    j = ptemp[j-1];
                }else{
                    ptemp[i] = 0;
                    i++;
                }
            }
        }

        return ptemp;
    }

    public static void main(String args[]){
        String text = "abcxabcdabcdabcy";
        String pattern = "abcdabcy";
        KMPPatternMatch kmp = new KMPPatternMatch();
        System.out.println("does pattern exists: "+kmp.search(text.toCharArray(),pattern.toCharArray()));
    }
}
