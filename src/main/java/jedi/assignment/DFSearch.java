package jedi.assignment;

import java.util.Iterator;
import java.util.LinkedList;

public class DFSearch {
    int v;
    LinkedList<Integer>[] adj;
    public DFSearch(int v){
        this.v = v;
        adj = new LinkedList[v];
        for(int i =0;i<v;i++)
            adj[i] = new LinkedList<>();
    }

    public void addEdge(int s, int d){
        adj[s].add(d);
    }

    public void dfsUtil(int s,boolean[] visited){
        visited[s] = true;
        System.out.println(s);
        Iterator<Integer> i = adj[s].listIterator();
        while(i.hasNext()){
            int n = i.next();
            if(!visited[n]){
                dfsUtil(n,visited);
            }
        }
    }

    public void dfs(int s){
        boolean[] visited = new boolean[v];
        dfsUtil(s,visited);
    }

    public static void main(String args[]){
        DFSearch dfSearch = new DFSearch(4);
        dfSearch.addEdge(0,1);
        dfSearch.addEdge(0,2);
        dfSearch.addEdge(1,2);
        dfSearch.addEdge(2,0);
        dfSearch.addEdge(2,3);
        dfSearch.addEdge(3,3);
        System.out.println("Following is depth first traversal starting from vertex 2");
        dfSearch.dfs(2);
    }
}
