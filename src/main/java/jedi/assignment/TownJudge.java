package jedi.assignment;

public class TownJudge {

    public int findJudge(int n, int[][] trust){
        int[] count = new int[n +1];
        for(int[] item :trust){
            count[item[0]]--;
            count[item[1]]++;
        }

        for(int i=1;i<n;i++)
            if(count[i] == n-1)
                return i;
        return -1;
    }

    public static void main(String args[]){
        TownJudge t = new TownJudge();
        int[][] trust = {
                {1,2},
                {3,2}
        };
        int judge = t.findJudge(3,trust);
        if(judge >0)System.out.println("town judge: "+judge);
    }

}
